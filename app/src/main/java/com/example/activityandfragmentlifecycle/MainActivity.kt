package com.example.activityandfragmentlifecycle

import android.content.ActivityNotFoundException
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil.setContentView
import com.example.activityandfragmentlifecycle.databinding.ActivityMainBinding
import timber.log.Timber
//
//outState.putInt("key_revenue", this.revenue)
//outState.putInt("key_desertSold", this.dessertsSold)
const val KEY_REVENUE = "key_revenue"
const val KEY_DESERT_SOLD = "key_desertSold"
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var revenue = 0
    private var dessertsSold = 0
    data class Dessert(val imageId: Int, val price: Int, val startProductionAmount: Int)
    private val allDesserts = listOf(
        Dessert(R.drawable.cupcake, 5, 0),
        Dessert(R.drawable.donut, 10, 5),
        Dessert(R.drawable.eclair, 15, 20),
        Dessert(R.drawable.froyo, 30, 50),
        Dessert(R.drawable.gingerbread, 50, 100),
        Dessert(R.drawable.honeycomb, 100, 200),
        Dessert(R.drawable.icecreamsandwich, 500, 500),
        Dessert(R.drawable.jellybean, 1000, 1000),
        Dessert(R.drawable.kitkat, 2000, 2000),
        Dessert(R.drawable.lollipop, 3000, 4000),
        Dessert(R.drawable.marshmallow, 4000, 8000),
        Dessert(R.drawable.nougat, 5000, 16000),
        Dessert(R.drawable.oreo, 6000, 20000)
    )
    private var currentDessert = allDesserts[0]
    private lateinit var desertTimer: DessertTimer

    /*one time initialization, dipanggil ulang hanya jika misal pindah config dari potrait ke landscape*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("onCreate")
        binding = setContentView(this, R.layout.activity_main)
        if (savedInstanceState != null) {
            this.revenue = savedInstanceState.getInt(KEY_REVENUE)
            this.dessertsSold = savedInstanceState.getInt(KEY_DESERT_SOLD)
        }
        binding.amountSold = this.dessertsSold
        binding.revenue = this.revenue
        binding.dessertButton.setOnClickListener { onDesertClicked() }
        binding.dessertButton.setImageResource(currentDessert.imageId)
        desertTimer = DessertTimer(this.lifecycle)
    }

    private fun onDesertClicked() {
        revenue += currentDessert.price
        dessertsSold++
        binding.revenue = revenue
        binding.amountSold = dessertsSold
        showCurrentDessert()
    }

    private fun showCurrentDessert() {
        var newDessert = allDesserts[0]
        for (dessert in allDesserts) {
            if (dessertsSold >= dessert.startProductionAmount) {
                newDessert = dessert
            } else break
        }
        if (newDessert != currentDessert) {
            currentDessert = newDessert
            binding.dessertButton.setImageResource(newDessert.imageId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shareMenuButton -> onShare()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onShare() {
        val shareIntent = ShareCompat.IntentBuilder.from(this)
            .setText(getString(R.string.share_text, dessertsSold, revenue))
            .setType("text/plain")
            .intent
        try {
            startActivity(shareIntent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(
                this, getString(R.string.sharing_not_available),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onStart() {
        super.onStart()
        Timber.i("on start")
    }

    override fun onResume() {
        super.onResume()
        Timber.i("on resume")
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        Timber.i("on resume fragment")
    }

    override fun onPostResume() {
        super.onPostResume()
        Timber.i("on post resume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("on destroy")
    }

    override fun onRestart() {
        super.onRestart()
        Timber.i("on restart")
    }

    override fun onStop() {
        super.onStop()
        Timber.i("on stop")
    }

    override fun onPause() {
        super.onPause()
        Timber.i("on pause")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.i("on save instance")
        outState.putInt(KEY_REVENUE, this.revenue)
        outState.putInt(KEY_DESERT_SOLD, this.dessertsSold)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Timber.i("on restore instance")
    }
}