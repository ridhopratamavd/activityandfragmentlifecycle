package com.example.activityandfragmentlifecycle

import android.app.Application
import timber.log.Timber

/*created before everything. so int his class we can plant timber so other class can use it later on*/
class PusherApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}